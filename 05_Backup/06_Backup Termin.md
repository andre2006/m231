# Backup prüfen - Jetzt einplanen!
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  15 Minuten |
| Ziele | Sie haben die regelmässige Backup-Überprüfung in Ihrem Terminkalender eingetragen. |

Toll, dass sie jetzt Backups für alle ihre Geräte bzw. Daten eingerichtet haben. Nebst der **regemässigen** Erstellung des Backups kommt ein weiterer ganz wichtiger Punkt dazu: Die **regelmässige** Überprüfung der Backups. Eine genau so traumatische Erfahrung, wie Daten zu verlieren, ist der Moment in dem Sie feststellen: Mein Backup hat gar nie funktioniert. Ein nicht **regelmässig** überprüftes Backup entspricht keinen Backup. **Prüfen Sie regelmässig und pflichtbewusst Ihre Backups!**

Empfohlen: Halbjährliche Überprüfung der Backups

Aufgabenstellung: Tragen Sie in ihrem Kalender einen wiederholenden Termin ein, der Sie daran erinnert das Backup zu überprüfen. 