# Begriffe Klären
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit, 4er Gruppen |
| Aufgabenstellung  | Theoretische Grundkenntnisse erarbeiten |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie kennen den Unterschied zwischen einem Block-Level und File-Level Backup |

Halten Sie die wesentlichen Informationen zu den einzelnen Themen fest und versuchen Sie die Fragen zu beantworten.
Das Ziel ist es, dass sie am Schluss der Klasse das Wesentlichste über das Thema erklären können.

## 3.1. Themen
 - 1. Thema: **Datensicherungsziele, Datenkompression**
   - Welche Medien/Datenträger/Geräte werden als Backup-Ziel verwenden?
   - Was für Kompressionsverfahren werden verwendet?
   - Weshalb ist Datenkompression insbesondere bei einem Image-Backup wichtig?
 - 2. Thema: **Vollsicherung, Differenzielle Sicherung, Inkrementelle Sicherung**
   - Erklären Sie den Unterschied zwischen den drei Methoden mithilfe einer Grafik
   - Wird für die differenzielle und inkrementelle Sicherung ein Vollbackup benötigt?
   - Vor- und Nachteile der verschiedenen Arten.
 - 3. Thema: **Block-Level vs File-Level Backup**
   - Dateisystem-Ebene: Kopieren von einzelnen Dateien vs. Kopieren von Datenblöcken
   - Erklären Sie die Begriffe und die Unterschiede.
   - Welche Methode eignet sich besser, wenn ein Image von einer Festplatte erstellt werden soll und weshalb?
   - Welche Methode eignet sich besser, wenn nur der Home-Folder gesichert werden muss?
 - 4. Thema: **Hot Backup vs Cold Backup**
   - Erklären Sie die Begriffe mithilfe jeweils einen oder zwei praktischen Einsatzzwecken
 - 5. Thema: **Datensicherungsstrategie**
   - Wie häufig soll man ein Backup machen?
   - Auf wie vielen unterschiedlichen Datenträger soll man Backups machen?
   - Begriffe zum Recherchieren: Turm von Hanoi, 3-2-1 Regel

## 3.2. Aufgabenstellung
Ziele: Jede Gruppe recherchiert zu einem der oben aufgeführten Themen und stellt die Erkenntnisse der Klasse vor.

 1. Die Klasse wird in 5 Gruppen aufgeteilt. Jede Gruppe behandelt ein Thema.
 3. 10 Minuten, Einzelarbeit: Recherchieren Sie zum Thema. Benutzen Sie dafür das Internet (Suchmaschine ihrer Wahl) und/oder die unten aufgeführten Links. 
 4. 20 Minuten: Tauschen Sie sich aus. Halten Sie die Erkenntnisse mit Stichworten oder in einer Grafik auf einem Poster fest. 
 5. Je (maximal!) 5 Minuten: Jede Gruppe stellt ihr Poster kurz der Klasse vor. 
 6. Halten Sie zu jedem Begriff die wichtigsten Erkenntnisse im Portfolio fest. 

## 3.3. Quellen
 - https://www.cloudbacko.com/blog/block-level-backup-vs-file-level-backup-which-is-better/
 - https://www.ubackup.com/backup-restore/block-level-backup-4348.html
 - https://de.wikipedia.org/wiki/Datensicherung