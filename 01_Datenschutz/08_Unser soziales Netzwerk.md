# Unser soziales Netzwerk
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Sozialen Netzwerk |
| Zeitbudget  |  1 Lektionen |
| Ziel | Perspektivenwechsel: Welche Ziele verfolge ich als Betreiber eines sozialen Netzwerkes?  |

## 9.1. Übersicht

Die Klasse wird in Gruppen à 3 bis 4 Personen aufgeteilt. Jede Gruppe kreiert ein soziales Netzwerk und "verkauft" (präsentiert) es der Klasse. Jede(r) Lernende stimmt für ein soziales Netzwerk ab (ausser das eigene).

## 9.2. Vier Phasen
 1. Kreativer Austausch - Sie kreieren ihr Soziales Netzwerk
 2. Sie bereiten die Präsentation Ihres Sozialen Netzwerkes vor. 
 3. Präsentation - Sie haben 3 Minuten um ihr Soziales Netzwerk der Klasse zu präsentieren. 
 4. Abstimmung - Die Klasse stimmt ab, welches Soziale Netzwerk gewinnt. 

## 9.3. Phase 1 - Kreativer Austausch (20 min)
Diskutieren Sie mit Ihren Kollegen und beantworten Sie diese Fragen für Ihr soziales Netzwerk. Halten Sie Ihre Antworten in Ihrem Portfolio fest. 
 1. Wie heisst das neue soziale Netzwerk? Was ist das Motto/Slogan?
 2. Was ist der Sinn und Zweck des Netzwerkes?
 3. Welche Posts/Einträge/Inhalte können hochgeladen und veröffentlicht werden?
 4. Was kann nicht veröffentlicht werden? Warum nicht?
 5. Welche Informationen kann/muss ein Nutzer von sich preisgeben, um sich registrieren zu können?
 6. Wie bewegen Sie potenzielle Nutzer dazu, sich ihrem Netzwerk anzuschliessen?
 7. Was ist der Unterschied zu den bereits bestehenden Netzwerken (z.B. Instagram, Facebook, Twitter, Pinterest, Xing, etc.)?
 8. Gibt es weitere Einschränkungen, was man auf dem Netzwerk darf und was nicht?
 9. Wie finanzieren Sie Ihr Soziales Netzwerk (Serverkosten, Löhne, usw.)?

## 9.4. Phase 2 - Vorbereitung der Präsentation (20 min)
Ziel ist es in 3 Minuten möglichst viele von Ihren Mitschülern von Ihrem Sozialen Netzwerk zu überzeugen. Die Präsentationsform steht Ihnen frei: Powerpoint, eine Animation, ein Poster, ein kurzer Sketch, ein kurzes Filmchen, eine Radiowerbung. Seien Sie kreativ!

## 9.5. Phase 3 - Präsentationsphase
Jede Gruppe führt Ihre Präsentation durch. Denken Sie daran: Wer sein Netzwerk am besten verkauft, gewinnt!

Nach ihrer 3 Minütigen Präsentation stehen der Klasse und der Lehrperson 5 Minuten für Fragen zur Verfügung.

## 9.6. Phase 4 - Abstimmung
Welches Konzept hat Sie überzeugt? Bei welchem Sozialen Netzwerk würden Sie gerne mitmachen?

Stimmen Sie ab. Natürlich NICHT für Ihr Eigenes!
