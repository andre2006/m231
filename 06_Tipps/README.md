# Tipps

# Lernjournal

Das Lernjournal hat folgende Ziele:
 - Lernzeit planen
 - Lernerfolge prüfen und auswerten

Dafür empfiehlt es sich für jeden Tag folgende Fragen zu beantworten:
 - Das möchte ich heute machen
 - Das habe ich heute gemacht
 - Das habe ich heute gelernt
 - Das ist für mich persönlich wichtig
 - Das ist mir gut gelungen
 - So bin ich vorgegangen, damit es mir gelingt.
 - Damit hatte ich Schwierigkeiten
 - So habe ich auf die Schwierigkeiten reagiert
 - So habe ich mich heute gefühlt.
 - Ich bin zufrieden mit meiner Leistung, weil...

In Markdown könnten Sie folgende Struktur verwenden. 

```
# Lernjournal von Max Müller zum Modul 231

## 03.01.2022
### Das möchte ich heute machen
Heute wurden uns in einer Präsentation das Thema Verschlüsselung vorgestellt. Dafür hat die Lehrperson uns Übungen zum Thema zusammengestellt: https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/06_encryption
Mein Ziel für heute ist es die Übungen *2.PGP* und *.Hash(funktion)* zu bearbeiten. Das Thema *5. Cryptomator* bearbeite ich zu Hause. 
### Das habe ich heute gemacht
PGP hat mir ein wenig mühe bereitet. Der Unterschied zwischen Public und Private Key war nicht ganz klar. Das hat mich Zeit gekostet. Deshalb konnte ich mit dem Thema Hash(funktion) erst begonnen. 
 - Installation [gpg4win](https://www.gpg4win.org/)
 - Private Key mit Kleopatra erstellt.

 
### Das habe ich heute gelernt
 - Unterschied zwischen Private und Public Key: Den Public Key gebe ich an den Sender weiter, damit dieser mir eine verschlüsselte Nachricht senden kann. 
### Das ist für mich persönlich wichtig
 - 
### Das ist mir gut gelungen
 - Die Installation und die Bedienung von Kleopatra
### So bin ich vorgegangen, damit es mir gelingt.
### Damit hatte ich Schwierigkeiten
### So habe ich auf die Schwierigkeiten reagiert
### So habe ich mich heute gefühlt.
### Ich bin zufrieden mit meiner Leistung, weil...


```