# Notwendigkeit von Passwörter - Was sind sichere Passwörter?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit, 3er Gruppen |
| Aufgabenstellung  | Austausch zum Thema Passwörter |
| Zeitbudget  |  20 Minuten |
| Ziel | Grundlagen für die Notwendigkeit von Passwörter verstehen. |

Erstellen Sie in 3er Gruppen auf einem A3 ein Mindmap zu den folgenden beiden zentralen Fragen (Die kursiv geschrieben Fragen dienen lediglich zum besseren Verständnis):

 - Warum Passwörter?<br><i>Grund für den Einsatz von Passwörter: Weshalb werden Passwörter eingesetzt?</i>
 - Was wird geschützt?<br><i>Was wird mit Passwörter geschützt?<br>Welche Arten von Informationen werden mit Passwörter geschützt, welche sind frei verfügbar?</i>
 - Was ist ein sicheres Passwort?

Fügen Sie dieses Mindmap in ihr Portfolio ein!

Setzen Sie sich mit den nachfolgenden Quellen auseinander und legen Sie fünf Passwort-Richtlinien fest. Halten Sie diese Regeln in ihrem Portfolio fest. 

## Quellen
 1. https://www.ncsc.gov.uk/blog-post/the-logic-behind-three-random-words
 2. https://arstechnica.com/information-technology/2019/06/microsoft-says-mandatory-password-changing-is-ancient-and-obsolete/
 3. https://blog.codinghorror.com/password-rules-are-bullshit/
 4. https://github.com/dumb-password-rules/dumb-password-rules
 5. https://pages.nist.gov/800-63-3/sp800-63b.html
 6. https://github.com/danielmiessler/SecLists/pull/155
