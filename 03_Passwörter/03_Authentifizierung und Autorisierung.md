# Authentifizierung und Autorisierung
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag, Beantworten von Verständnisfragen |
| Zeitbudget  |  15 Minuten |
| Ziel | Den Unterschied zwischen den Begriffen  Authentifizierung und Autorisierung verstehen. |

Zur Anmeldung an einem Computer oder Netzwerk müssen Sie Ihren Benutzernamen und Ihr Kennwort eingeben. Dieser Vorgang wird Authentifizierung genannt. Er ermöglicht es, einen
Netzwerkbenutzer (eindeutig, wenn jeder Benutzer über ein eigenes Konto verfügt) einer bestimmten Person zuzuordnen.

Nach erfolgreicher Authentifizierung wird in Form der Autorisierung überprüft, welche Zugriffsrechte ein Benutzer im Netzwerk hat bzw. ob dieser die Erlaubnis hat, Software selbstständig zu installieren. Oder hier nochmal ausführlicher: [Dr. Datenschutz](https://www.dr-datenschutz.de/authentisierung-authentifizierung-und-autorisierung/)

Ordnen Sie die folgenden Aussagen die Begriffe Authentifizierung und Autorisierung zu. Nutzen Sie dafür dieses [MS Forms Quiz](https://forms.office.com/r/gMjdrcM7Da).
 - "Peter hat das Recht auf den Ordner *Geschäftsprozesse* zuzugreifen."
 - "Martina sperrt ihren Bildschirm vor der Kaffeepause und muss ihr Passwort eingeben, wenn Sie zurückkommt."
 - "Standard User haben keinen Zugriff auf die Server-Management-Konsole."
 - "Standardmässig kann in Linux Dateisystemen für jede Datei oder Ordner ein Benutzer, eine Gruppe, sowie die Zugriffsberechtigungen Lesen, Schreiben und Ausführen definiert werden."
 - RADIUS Server
 - "Nur Lehrerpersonen können in MS Teams einer Gruppe Lernende hinzufügen oder entfernen."
 - "Permission denied."
 - "Für das Online-Banking benötige ich mein Benutzername, mein Passwort und das Foto-TAN App."